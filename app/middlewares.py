from aiohttp import web

from utils import json_response
from utils.logger import create_logger


logger = create_logger('app.log', __name__)


@web.middleware
async def cors_middleware(request, handler):
    configs = request.app['configs']

    headers = {
        'Access-Control-Allow-Origin': configs.app_headers_allow_origin,
        'Access-Control-Allow-Credentials': configs.app_headers_allow_credentials,
        'Access-Control-Allow-Headers': configs.app_headers_allow_headers,
        'Access-Control-Allow-Methods': configs.app_headers_allow_methods,
    }

    if request.method == 'OPTIONS':
        response = web.Response()
    else:
        response = await handler(request)
    response.headers.update(headers)

    return response


@web.middleware
async def exceptions_handler(request, handler):
    def error(e, status):
        logger.error(f'{status}: error catched {e}')
        return json_response(data={ "error": str(e) }, status=status)

    try:
        result = await handler(request)
    except Exception as e:
        logger.error(f'error catched {e}')
        raise Exception

    return result

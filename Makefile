SHELL := /bin/bash

dev:
	python run_dev.py

dev_no_db:
	ENV='dev' python run_dev.py

prod:
	gunicorn -D 'app.main:make_app' --workers=3 --bind 0.0.0.0:8080 --worker-class aiohttp.worker.GunicornWebWorker --access-logfile app_access.log --error-logfile app_error.log

tests:
	ENV='test' pytest -rs -v


.PHONY: dev tests prod

from aiohttp import web
import ffmpeg
from utils import json_response
import os
import re
import uuid
from urllib.parse import urljoin

from tinytag import TinyTag
from tinytag.tinytag import TinyTagException

routes = web.RouteTableDef()

video_exts_reg = re.compile(r'\.(WEBM|MKV|MPG|MP2|MPEG|MPE|MPV|OGG|MP4|M4P|M4V|AVI|WMV|MOV|FLV|SWF)$', flags=re.IGNORECASE)


def extract_video_meta(_file):
    print(ffmpeg.probe(_file['realpath']))

    try:
        video_file=TinyTag.get(_file['realpath'], image=True)
        video_file.get_image()

        _file.update({'meta': video_file.as_dict()})
        _file.update({'probe': ffmpeg.probe(_file['realpath'])})

        return _file
    except TinyTagException:
        return _file


@routes.get('/ping')
async def ping(request):
    return json_response({'result': 'pong'})


@routes.get('/films')
async def get_all_films(request):
    def create_file_info(dirpath, filename):
        file_local_path = os.path.join(dirpath.replace(library_root_dir, ''), filename)

        realpath = os.path.join(dirpath, filename)

        return {
            'id': str(uuid.uuid3(uuid.NAMESPACE_DNS, realpath)),
            'realpath': realpath,
            'filename': filename,
            'ext': filename.split('.')[-1],
            'url': urljoin(library_host, file_local_path),
        }

    def get_files_list(dir, result):
        for (dirpath, dirnames, filenames) in os.walk(dir):
            videos = [create_file_info(dirpath, filename) for filename in filenames if re.search(video_exts_reg, filename)]
            if videos:
                result.extend(videos)
            if dirnames:
                for dirname in dirnames:
                    get_files_list(dirname, result)

        return result

    configs = request.app['configs']
    library_root_dir = configs.library_root_dir
    library_host = f'//{configs.library_hostname}:{configs.library_port}'

    files_list = get_files_list(library_root_dir, [])

    return json_response({'videos': list(map(extract_video_meta, files_list))})

# @routes.get('/test')
# async def stream_video(request):
#     response = web.StreamResponse()
#     await response.prepare(request)

#     ffmpeg_processor = ffmpeg.input('1280.avi').output('pipe:', format='webm', preset='ultrafast').overwrite_output()
#     # ffmpeg_processor = ffmpeg.input('1280.avi').output('pipe:', format='webm')
#     ffmpeg_processor = ffmpeg.input('1280.avi').output('out.mp4', format='mp4', preset='ultrafast').overwrite_output()

#     video = ffmpeg_processor.run_async(pipe_stdout=True)

#     for _, line in enumerate(video.stdout, 1):
#         await response.write(line)

#     await response.write_eof()

#     return response







import asyncio
import ffmpeg_streaming
from ffmpeg_streaming import Formats

@routes.get('/test')
async def stream_video(request):
    response = web.StreamResponse()
    response.content_type = 'multipart/x-mixed-replace; boundary=frame'
    await response.prepare(request)

    video = ffmpeg_streaming.input('1280.avi')
    dash = video.dash(Formats.h264())
    dash.auto_generate_representations()
    return await dash.output('/var/media/dash.mpd')

    import pdb; pdb.set_trace()


    return response

    print(ffmpeg.probe('1280.avi'))

    # ffmpeg_processor = ffmpeg.input('1280.avi').output('pipe:', format='webm', preset='ultrafast').overwrite_output()
    # ffmpeg_processor = ffmpeg.input('1280.avi').output('pipe:', format='webm')
    ffmpeg_processor = ffmpeg.input('1280.avi').output('out.mp4', format='mp4', preset='ultrafast').overwrite_output()

    ffmpeg_processor.run_async(pipe_stdout=True, quiet=True)

    count = 0

    await asyncio.sleep(1)

    with open('out.mp4', 'rb') as video:
        for line in video:
            await response.write(line)



        # while True:
        #     count += 1

        #     # Get next line from file
        #     line = video.readline()

        #     # if line is empty
        #     # end of file is reached
        #     if not line:
        #         break

        #     await response.write(line)

    await response.write_eof()

    return response




def get_routes():
    return routes

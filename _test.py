import ffmpeg_streaming
from ffmpeg_streaming import Formats

# video = ffmpeg_streaming.input('out.mp4')
video = ffmpeg_streaming.input('1280.avi')
dash = video.dash(Formats.h264())
dash.auto_generate_representations()
dash.generate_hls_playlist()
dash.output('res/dash.mpd')

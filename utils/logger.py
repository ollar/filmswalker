import os
import logging
from logging.handlers import RotatingFileHandler


LOGS_DIR = os.path.join(os.getcwd(), 'logs')


def create_logger(log_filename, logger_name, log_level=logging.DEBUG, logs_path=''):
    logs_dir = os.path.join(LOGS_DIR, logs_path)
    os.makedirs(logs_dir, mode=0o777, exist_ok=True)

    open(os.path.join(logs_dir, log_filename), 'a').close()

    handler = RotatingFileHandler(os.path.join(logs_dir, log_filename), maxBytes=1024 * 1024 * 5, backupCount=2)
    formatter = logging.Formatter('%(asctime)s %(name)-20s %(levelname)-8s %(message)s')

    handler.setFormatter(formatter)
    handler.setLevel(log_level)

    logger = logging.getLogger(logger_name)
    logger.addHandler(handler)
    logger.setLevel(log_level)

    return logger

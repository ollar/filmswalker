from aiohttp import web
import asyncio
# import uvloop

from .routes import get_routes
from .configs import Configs
from .middlewares import cors_middleware, exceptions_handler
from utils.logger import create_logger


# asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

logger = create_logger('app.log', __name__)


async def on_startup(app):
    logger.debug('app on_startup run')


async def on_shutdown(app):
    logger.debug('app is cleaning up')


async def make_app(**options):
    logger.debug('loading configs')
    configs = Configs(**options)

    logger.debug('initializing main app')
    app = web.Application(
        middlewares=[
            cors_middleware,
            # exceptions_handler,
        ]
    )

    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)

    app['configs'] = configs

    logger.debug('adding main routes')
    app.add_routes(get_routes())

    return app


if __name__ == "__main__":
    web.run_app(make_app())

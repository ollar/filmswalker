from aiohttp import web
import json
from json.decoder import JSONDecodeError
from functools import partial
from datetime import datetime


def default_json(o):
    if isinstance(o, datetime):
        return o.timestamp() * 1000
    return json.JSONEncoder.default(o)  # pylint: disable=no-value-for-parameter


json_dumps = partial(json.dumps, default=default_json)
json_response = partial(web.json_response, dumps=json_dumps)


import os
from dotenv import dotenv_values


config = dotenv_values(".env")

ENV = os.getenv('ENV', 'prod')

settings = {}

class Configs():
    ENV = ENV

    def __init__(self, **options):
        settings.update(config)
        settings.update(options)

        for key, value in settings.items():
            setattr(self, key, value)
